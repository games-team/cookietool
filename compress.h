/*
    cookietool is (c) 1995-2001 by Wilhelm Noeker (wnoeker@t-online.de)

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
    General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
    02111-1307 USA

 */


/*========================================================================*\
 |  File: compress.h                                   Date: 22 Mar 2001  |
 *------------------------------------------------------------------------*
 |     Read cookies, remove duplicates, sort, and write back to file.     |
 |       These routines are common to both cookietool and cdbdiff.        |
 |                                                                        |
\*========================================================================*/


#include "strstuff.h"


extern long listed;             /* number of cookies in memory */

void read_cookies   ( FILE *fp, int fmt );
void write_cookies  ( FILE *fp, int fmt, long offset );
void one_cookie     ( int delmode, int sortmode, UBYTE *hooktarget, FILE *fp, int fmt );

/* values for delmode */
enum {
    DUPDEL_NONE,        /* don't delete anything */
    DUPDEL_MATCH,       /* delete duplicates */
    DUPDEL_ABBREVS      /* delete duplicates and abbreviations */
    };

/* values for sortmode */
enum {
    SORT_REVERSE,       /* (only for use inside this module) */
    SORT_RESTORE,       /* restore original order */
    SORT_SIZE,          /* sort by size */
    SORT_BODY,          /* sort by body text */
    SORT_LASTLINE,      /* sort by last line */
    SORT_LASTWORD,      /* sort by last word */
    SORT_HOOKTARGET     /* sort by last occurence of special hook target */
    };
