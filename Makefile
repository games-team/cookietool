# $Id: Makefile,v 1.6 2000/06/17 18:49:22 baran Exp $
# slightly rewritten original Wilhelm Noeker's Makefile

targets		= cookietool cdbsplit cdbdiff
objects		= cookietool.o cdbsplit.o cdbdiff.o strstuff.o \
                  cookio.o compress.o
prefix		= usr
binprefix	= $(prefix)/games
manprefix	= $(prefix)/share/man/man6
CC		= gcc
RM		= rm -v
INSTALL		= install
CFLAGS		= -O2 -Wall

build : $(targets)

strstuff.o : strstuff.c strstuff.h
cookio.o   : cookio.c  cookio.h
compress.o : compress.c compress.h

cookietool   : cookietool.o strstuff.o compress.o cookio.o
cookietool.o : cookietool.c strstuff.h compress.h cookio.h
                                  
cdbdiff   : cdbdiff.o strstuff.o compress.o cookio.o
cdbdiff.o : cdbdiff.c strstuff.h compress.h cookio.h
                                  
cdbsplit   : cdbsplit.o strstuff.o cookio.o
cdbsplit.o : cdbsplit.c strstuff.h cookio.h

clean :
	@-$(RM) $(targets) $(objects)

# for AmigaOS installation change install-binary-unix to
# install-binary-amiga; for non-debian-installation, use `make all'

install : install-binary # install-manpages

install-binary :
	@for file in $(targets); do \
	  $(INSTALL) -m 0755 $$file $(DESTDIR)/$(binprefix)/$$file; \
	done;

install-manpages :
	@for file in doc/*.6; do \
	  $(INSTALL) -m 0644 $$file $(DESTDIR)/$(manprefix)/$$file; \
	done;

all : build install install-manpages
