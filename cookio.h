/*
    cookietool is (c) 1995-2001 by Wilhelm Noeker (wnoeker@t-online.de)

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
    General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
    02111-1307 USA

 */

/*========================================================================*\
 |  File: cookio.h                                     Date: 17 Feb 2001  |
 *------------------------------------------------------------------------*
 |      Generalised I/O for cookies, supporting various file formats.     |
 |                                                                        |
\*========================================================================*/


/* Cookie file formats: */
#define FMT_WORDS       0       /* cookies separated by arbitrary white space */
#define FMT_LINES       1       /* each line is a cookie */
#define FMT_PERCENT     2       /* cookies separated by "%" lines */
#define FMT_DBLPERCENT  3       /* cookies separated by "%%" lines */

#define DEF_FMT  2              /* default cookie format */


/* The static input buffer size, */
/* read_cookie() will never return a cookie larger than this. */
#define CBUFSIZE 64000L

char *read_cookie( FILE *fp, int fmt, long *psize, long *plines, long *pmaxline );
int  write_cookie( char *cookie, FILE *fp, int fmt );
void print_fmtinfo( int fmt );
